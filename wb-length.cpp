//https://gist.github.com/jadonk/2587524

// example:
//    $ nice -n-19 ./wiegand_test_epoll 5 6
//       here
//     green/data0 is gpio 5 (R4 at WB3.3)
//     white/data1 is gpio 6 (R3 at WB3.3)

// to compile:
//      arm-linux-gnueabi-gcc wiegand_test_epoll.c -o wiegand_test_epoll


#define GPIO_DATA0 5
#define GPIO_DATA1 6


#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <stdlib.h>
#include <sys/epoll.h>
#include <sys/types.h>
#include <signal.h>
#include <stdlib.h>
#include <string>
#include <ctime>
#include <sys/time.h>

#define __STDC_FORMAT_MACROS
#include <inttypes.h>

typedef long long int64; typedef unsigned long long uint64;

uint64 GetTimeMs64()
{
 
 struct timeval tv;

 gettimeofday(&tv, NULL);

 uint64 ret = tv.tv_usec;
 /* Convert from micro seconds (10^-6) to milliseconds (10^-3) */
 ret /= 1000;

 /* Adds the seconds (10^0) after converting them to milliseconds (10^-3) */
 ret += (tv.tv_sec * 1000);
 
/* Adds the seconds (10^0) after converting them to microseconds (10^-4) */
 //ret += (tv.tv_sec * 10000);

 return ret;

}



//#include <iostream>

//bool falling = 0;


//POSIX Обработка Segmentation Fault
int memento()
{
	int a=0;
	printf("Memento mori POSIX Signal");
	return 0;
}
void fall() // Testovoe Padenie
{
	  int* p = 0x00000000; 
	  *p = 13;
}
void posix_death_signal(int signum)
{
	memento(); 		 // прощальные действия
	signal(signum, SIG_DFL); // перепосылка сигнала
	exit(3); 		 //выход из программы. Если не сделать этого, то обработчик будет вызываться бесконечно.
}
//POSIX--


int init_gpio(int gpio) {
	// export gpio to userspace
	FILE * tmpf = fopen("/sys/class/gpio/export", "w");
	char path[42];
	fprintf(tmpf, "%d\n", gpio);
	fclose(tmpf);

	// set output direction
	sprintf(path, "/sys/class/gpio/gpio%d/direction", gpio);
	tmpf = fopen(path, "w");
	fprintf(tmpf, "%s\n", "in");
	fclose(tmpf);



	sprintf(path, "/sys/class/gpio/gpio%d/value", gpio);
	int fd = open(path, O_RDWR | O_NONBLOCK);
	if (fd <= 0) {
		fprintf(stderr, "open of gpio %d returned %d: %s\n", gpio, fd, strerror(errno));
	}
	return fd;

}


int edge_rising(int gpio) {

	char path[42];
	sprintf(path, "/sys/class/gpio/gpio%d/edge", gpio);
	FILE * tmpf = fopen(path, "w");
	fprintf(tmpf, "%s\n", "rising");
	fclose(tmpf);


	sprintf(path, "/sys/class/gpio/gpio%d/value", gpio);
	int fd = open(path, O_RDWR | O_NONBLOCK);
	if (fd <= 0) {
		fprintf(stderr, "open of gpio %d returned %d: %s\n", gpio, fd, strerror(errno));
	}
	return fd;
}



int edge_falling(int gpio) {

	char path[42]; 
	sprintf(path, "/sys/class/gpio/gpio%d/edge", gpio);
	FILE * tmpf = fopen(path, "w");
	fprintf(tmpf, "%s\n", "falling");
	fclose(tmpf);


	sprintf(path, "/sys/class/gpio/gpio%d/value", gpio);
	int fd = open(path, O_RDWR | O_NONBLOCK);
	if (fd <= 0) {
		fprintf(stderr, "open of gpio %d returned %d: %s\n", gpio, fd, strerror(errno));
	}
	return fd;
}


int main(int argc, char** argv) {
	signal(SIGSEGV, posix_death_signal);

	int n0, n1;
	int epfd0, epfd1;
	int fd_d0, fd_d1;

	if (argc != 3) {
		fprintf(stderr, "USAGE: %s <GPIO_D0> <GPIO_D1>\n", argv[0]);
		return 2;
	}

	int gpio_d0 = atoi(argv[1]);
	int gpio_d1 = atoi(argv[2]);
	fprintf(stderr, "Using GPIO %d for D0 and GPIO %d for D1\n", gpio_d0, gpio_d1);


//	epfd = epoll_create(1);




	fd_d0 = init_gpio(gpio_d0);
	fd_d1 = init_gpio(gpio_d1);





//	fd_d0 = init_gpio(6);
//	fd_d1 = init_gpio(7);
	
	//edge_rising(gpio_d0);
	//edge_rising(gpio_d1);
	edge_falling(gpio_d0);
	edge_falling(gpio_d1);

	if( !(fd_d0 > 0) || !(fd_d1 > 0)) {
		fprintf(stderr, "error opening gpio sysfs entries\n");
		return 1;
	}

    char buf = 0;

    epfd0 = epoll_create(1);
    epfd1 = epoll_create(1);
    struct epoll_event ev_d0, ev_d1;
    struct epoll_event events0;
    struct epoll_event events1;
    //struct epoll_event events[10];
    ev_d0.events = EPOLLET;
    //ev_d0.events = EPOLLPRI;
    ev_d1.events = EPOLLET;
    //ev_d1.events = EPOLLPRI;

    ev_d0.data.fd = fd_d0;
    ev_d1.data.fd = fd_d1;

    n0 = epoll_ctl(epfd0, EPOLL_CTL_ADD, fd_d0, &ev_d0);
    if (n0 != 0) {
		fprintf(stderr, "epoll_ctl returned %d: %s\n", n0, strerror(errno));
		return 1;
	}

    n1 = epoll_ctl(epfd1, EPOLL_CTL_ADD, fd_d1, &ev_d1);
    if (n1 != 0) {
		printf("epoll_ctl returned %d: %s\n", n1, strerror(errno));
		return 1;
	}


	size_t i	;

	unsigned int value = 0;
	unsigned bit_counter = 0;
	bool falling = 0;
	bool rising = 1;
	bool t1_start = 0;
	bool t2_start = 0;
	bool t3_start = 0;
	bool sn1, sn2 = 0;
	n0, n1 = 0;
	bool need_to_change = 1;
	uint64_t   t1,t2,t3,t4;
	uint64_t   time_t1_start_t2_start;
	uint64_t   time_t1_stop_t3_start;
	uint64_t   time_t2_stop_t4_start;
	uint64_t   time_t3_stop_t4_stop;

	uint64_t ui64 = GetTimeMs64();
	printf("test uint64_t : %" PRIu64 "\n", ui64);

	unsigned int Speed1, Speed2;
	unsigned int Length1, Length2;

    while(1) {
	
      if(need_to_change){

	if (falling) {
	edge_falling(gpio_d0);
	edge_falling(gpio_d1);
	printf("Change to Falling"); 
	}		
	
	if (rising) {
	edge_rising(gpio_d0);
	edge_rising(gpio_d1);
	printf("Change to Rising"); 
	}
	need_to_change = 0;
      }
    
      n0 = epoll_wait(epfd0, &events0, 1, 1);
      //printf("n0 epoll returned %d: %s\n", n0, strerror(errno));

      n1 = epoll_wait(epfd1, &events1, 1, 1);
      //printf("n1 epoll returned %d: %s\n", n0, strerror(errno));
	

      if(n0 > 0) {
       if (falling) {
	printf("F gpio0 %d\n1", n0); 
	t1_start = 1;
	time_t1_start_t2_start = GetTimeMs64();
	sn1 = 1;
	} else {
	printf("R gpio0 %d\n1", n0); 
	sn1 = 0;
	t2_start = 1;
	
	time_t2_stop_t4_start = GetTimeMs64();
	

	}
      }
    

      if(n1 > 0) {
       if (falling) {
	printf("F gpio1 %d\n1", n1);
	t1_start = 0; 

	time_t1_stop_t3_start = GetTimeMs64();

	
	
	sn2 = 1;
	} else {
	printf("R gpio1 %d\n1", n1); 
	sn2 = 0;
	t2_start = 0;
	t3_start = 0;
	
	
	time_t3_stop_t4_stop = GetTimeMs64();
	
//	   time_t curtime;

//	   time(&curtime);

//	   printf("Current time = %s", ctime(&curtime));

	t1 = time_t1_stop_t3_start - time_t1_start_t2_start;   
	t2 = time_t2_stop_t4_start - time_t1_start_t2_start;   
	t3 = time_t3_stop_t4_stop  - time_t1_stop_t3_start;   
	t4 = time_t3_stop_t4_stop  - time_t2_stop_t4_start;   
	
	printf("t1 uint64_t : %" PRIu64 "\n", t1);
	printf("t2 uint64_t : %" PRIu64 "\n", t2);
	printf("t3 uint64_t : %" PRIu64 "\n", t3);
	printf("t4 uint64_t : %" PRIu64 "\n", t4);

	Speed1 = 10000 / t1;
	Speed2 = 10000 / t4;
	
	Length1 = Speed1 * t2;
	Length2 = Speed2 * t3;


	printf("Speed1 : %d\n", Speed1);
	printf("Speed2 : %d\n", Speed2);
	printf("Length1 : %d\n", Length1);
	printf("Length2 : %d\n", Length2);

	}
      }


	if(sn1 && sn2 && !t3_start && falling) { 
		t3_start = 1;	
		
//		time_t1_stop_t3_start = GetTimeMs64();
		rising = 1;
		falling = 0;
		need_to_change = 1;	
		printf("Need Change Input Edge to Rising"); 
	}

	if (!sn1 && !sn2 && rising) {
		rising = 0;
		falling = 1;
		need_to_change = 1;
		printf("Need Change Input Edge to Falling");	
	}


//		n = epoll_wait(epfd, events, 1, 1000);

//		for ( i = 0;  i < n; ++i) {
//			value <<= 1;
//			if (events[i].data.fd == ev_d1.data.fd) {
//				value |= 0x01;
//			}
//
//			bit_counter += 1;
//		}
//
//		if (bit_counter && (n == 0)) {
//			printf("got value %d = 0x%x (%d bits)\n", value, value, bit_counter);
//			value = 0;
//			bit_counter = 0;
//		}
//
//

//	for (i = 0; i< n; ++i) {
//	bit_counter += 1;
//	
//	}
//
//	if (bit_counter && (n == 0)) {
//	
//		printf("--");
//
//
//	}
//
    }


  return(0);
}
